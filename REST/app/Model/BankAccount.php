<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class BankAccount extends Model
{
    protected $table = "bank_account";
    
    protected $fillable = [
    	"account_type_id",
    	"account_nickname",
    	"account_number",
    	"balance",
    	"user_id"
    ];
    
    public static function create(array $attributes=Array()) {
    	$i = 0;
    	$accountNum = mt_rand(1, 9);
    	do {
    		$accountNum .= mt_rand(0, 9);
    	} while (++$i < 14);
    	
    	$attributes["account_number"] = $accountNum;
    	
    	parent::create($attributes);
    }
    
    public function AccountType() {
    	return $this->hasOne("App\Model\AccountType", "id");
    }
}
