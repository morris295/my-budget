<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TransactionStatus extends Model
{
    protected $table = "transaction_status";
    
    protected $guarded = [
    	"id", "description"
    ];
}
