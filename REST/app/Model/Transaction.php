<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $table = "transaction";
    
    protected $fillable = [
    	"transaction_type_id",
    	"account_id",
    	"description",
    	"amount",
    	"comments",
    	"transaction_status_id",
    	"posting_date"
    ];
    
    public static function create(array $attributes = Array()) {
    	
    	$account = BankAccount::find($attributes["account_id"]);
    	$attributes["posting_date"] = date("Y-m-d H:i:s", strtotime($attributes["posting_date"]));
    	
    	if ($attributes["transaction_type_id"] == 1) {
    		//Credit
    		$account["balance"] += $attributes["amount"];
    	} else if ($attributes["transaction_type_id"] == 2) {
    		//Debit
    		$account["balance"] -= $attributes["amount"];
    	}
    	
    	$account->save();
    	parent::create($attributes);	
    }
    
    public function transactionType() {
    	return $this->hasOne("App\Model\TransactionType", "id", "transaction_type_id");
    }
    
    public function transactionStatus() {
    	return $this->hasOne("App\Model\TransactionStatus", "id", "transaction_status_id");
    }
    
    public function account() {
    	return $this->hasOne("App\Model\BankAccount", "id", "account_id");
    }
}
