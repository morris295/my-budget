<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\BankAccount;
use Ramsey\Uuid\Generator\MtRandGenerator;

class BankAccountController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    	$content = $request->all();
    	$data = BankAccount::create($content);
    	return response()->json($data);
    	
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
    	$data = BankAccount::with("AccountType")->get()->toJson();
    	
    	return response()->json($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    	$content = $request->all();
    	$account = BankAccount::find($id);
    	$account->account_nickname = $content["account_nickname"];
    	$account->save();
    	
    	return response()->json($account);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {   
    	$data = BankAccount::destroy($id);
    	return response()->json($data);
    }
}
