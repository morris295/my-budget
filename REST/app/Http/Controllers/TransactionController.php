<?php

namespace App\Http\Controllers;

use App\Model\Transaction;
use Illuminate\Http\Request;

class TransactionController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $content = $request->all();
        $response = [];
        
        try {
        	
        	$response["code"] = 201;
        	$response["data"] = Transaction::create($content);
        	
        } catch (Exception $e) {
        	
        	$response["code"] = 501;
        	$response["message"] = $e->getMessage();
        	
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    	return Transaction::find($id)->toJson();
    }
    
    /**
     * Display all transactions for the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showAll($accountId)
    {
    	return Transaction::with("Account", "TransactionType", "TransactionStatus")
    	->where("account_id", $accountId)
    	->get()
    	->toJson();
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    	$response = [];
    	$response["code"] = 201;
    	$response["data"] = Transaction::destroy($id);
    	return json_encode($response);
    }
}
