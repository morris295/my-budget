<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBankAccountTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    	Schema::create('bank_account', function (Blueprint $table) {
    		$table->increments('id');
    		$table->integer('account_type_id');
    		$table->integer('user_id');
    		$table->string('account_nickname');
    		$table->bigInteger('account_number');
    		$table->decimal('balance',13,2);
    		$table->timestamps();
    	});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    	Schema::drop('account');
    }
}
