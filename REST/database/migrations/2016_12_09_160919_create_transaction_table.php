<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    	Schema::create('transaction', function(Blueprint $table) {
    		$table->increments('id');
    		$table->integer('transaction_type_id');
    		$table->integer('account_id');
    		$table->string('description');
    		$table->decimal('amount', 13, 2);
    		$table->string('comments');
    		$table->integer('transaction_status_id');
    		$table->dateTime('posting_date');
    		$table->timestamps();
    	});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    	Schema::drop('transaction');
    }
}
