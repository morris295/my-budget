<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');

Route::get("/v1/accounts/", "BankAccountController@show");
Route::post("v1/accounts/", "BankAccountController@store");
Route::put("v1/accounts/{id}", "BankAccountController@update");
Route::delete("v1/accounts/{id}", "BankAccountController@destroy");

Route::get("v1/transaction/all/{accountId}", "TransactionController@showAll");
Route::get("v1/transaction/{transactionId}", "TransactionController@show");
Route::post("v1/transaction", "TransactionController@store");
Route::put("v1/transaction/{transactionId}", "TransactionController@update");
Route::delete("v1/transaction/{transactionId}", "TransactionController@destroy");