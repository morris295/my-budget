# My Budget

- There is a "misc" directory within the "REST" directory which contains an "example.env" file.  This file contains the DB connection details, rename to ".env" and it should provide the db connection.  Running "php artisan migrate" should then create the database and tables.
- Also in the "misc" directory is a "my_budget_schema.sql" file.  This can be used as an alternative to "php artisan migrate".
- Lastly, the "misc" directory contains a directory called "my_budget_seed_data".  This contains a set of .csv files that can be used to seed the database with test data.
- Finally, both "composer install" and "bower_install" should be run to get any dependencies before trying to run the app.