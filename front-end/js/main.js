var app = angular.module('myBudget', [
	'angularSpinner',
	'jcs-autoValidate',
	'angular-ladda',
	'mgcrea.ngStrap',
	'toastr',
	'ngAnimate',
	'ui.router'
]);

app.run(function(defaultErrorMessageResolver) {
	defaultErrorMessageResolver.getErrorMessages().then(function(errorMessages) {
		errorMessages["currencyFormat"] = "You must enter a valid currency value, e.g. 14.99";
	});
});

app.config(function ($stateProvider, $urlRouterProvider) {
	$stateProvider
		.state('list', {
			url: "/",
			views: {
				'main': {
					templateUrl: 'templates/account-list.html',
					controller: 'AccountListController'
				}
			}
		})
		.state('transaction', {
			url: "/transactions/:accountId",
			views: {
				'main': {
					templateUrl: 'templates/transactions.html',
					controller: 'TransactionListController'
				}
			}
		})
		.state('create-account', {
			url: "/account/create",
			views: {
				'main': {
					templateUrl: 'templates/createaccount.html',
					controller: 'AccountCreateController'
				}
			}
		})
		.state('create-transaction', {
			url: "/transaction/create/:accountId",
			views: {
				'main': {
					templateUrl: 'templates/createtransaction.html',
					controller: 'TransactionCreateController'
				}
			}
		});

	$urlRouterProvider.otherwise('/');
});

app.config(function ($httpProvider, laddaProvider, $datepickerProvider) {
	laddaProvider.setOption({
		style: 'expand-right'
	});
	angular.extend($datepickerProvider.defaults, {
		dateFormat: 'M/d/yyyy',
		autoclose: true
	});
});

app.controller('AccountListController', function ($scope, $state, AccountService) {
	$scope.accounts = AccountService;
});

app.controller('TransactionListController', function($scope, $state, $stateParams, TransactionService) {
	$scope.transactions = TransactionService;
	if ($stateParams.accountId !== "") {
		$scope.transactions.setAccountParam($stateParams.accountId);
	} else {
		$state.go("list");
	}
	$scope.transactions.loadTransactions();
});

app.controller('AccountCreateController', function($http, $scope, $state, $timeout, toastr, AccountService) {
	$scope.form = {};
	$scope.accountService = AccountService;
	
	$scope.save = function() {
		var data = {
			user_id: 1,
			account_type_id: $scope.form.accountType,
			account_nickname: $scope.form.accountNickname,
			balance: $scope.form.accountBalance
		};
		
		$scope.accountService.isSaving = true;
		
		$timeout(function() {
			$scope.accountService.addAccount(data)
			.then(function(result) {
				$scope.accountService.isSaving = false;
				toastr.success("Success", "Account Added.");
			});
		}, 2000);
	}
});

app.controller('TransactionCreateController', function ($http, $scope, $state, $stateParams, $timeout, toastr, TransactionService) {
	$scope.accountId = $stateParams.accountId;
	$scope.form = {};
	$scope.transactionService = TransactionService;
	
	$scope.save = function() {
		var year = $scope.form.transactionPostingDate.getFullYear();
		var day = $scope.form.transactionPostingDate.getDate();
		var month = $scope.form.transactionPostingDate.getMonth()+1;
		var date = year + "/" + month + "/" + day;
		
		var data = {
			account_id: $scope.accountId,
			transaction_type_id: $scope.form.transactionType,
			transaction_status_id: $scope.form.transactionStatus,
			description: $scope.form.transactionDescription,
			amount: $scope.form.transactionAmount,
			comments: $scope.form.transactionComments,
			posting_date: date
		};
		$scope.transactionService.isSaving = true;
		
		$timeout(function() {
			$scope.transactionService.addTransaction(data)
			.then(function(result) {
				$scope.transactionService.isSaving = false;
				toastr.success("Success", "Account Added.");
			});
		}, 2000);
	};
});

app.service('AccountService', function ($http, $rootScope, $q) {

	var self = {
		baseUrl: "http://localhost/my_budget/public/api/v1/accounts",
		isSaving: false,
		isLoading: false,
		assets: [],
		loadAccounts: function() {
			self.isLoading = true;
			var params = null;
			$http({
				method: "GET",
				url: self.baseUrl
			})
			.then(function(result) {
				var items = angular.fromJson(result.data);
				angular.forEach(items, function(item) {
					self.assets.push(item);
				});
				self.isLoading = false;
			}, 
			function(err) {
				console.error(err);
			});
		},
		addAccount: function(params) {
			var promise = $http({
				method: "POST",
				url: self.baseUrl,
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				data: $.param(params)
			});
			return promise;
		}
	};

	self.loadAccounts();

	return self;

});

app.service('TransactionService', function($http, $rootScope, $q, toastr, $timeout) {
	
	var self = {
		baseUrl: "http://localhost/my_budget/public/api/v1/transaction",
		isSaving: false,
		isLoading: false,
		transactionList: [],
		accountParam: 0,
		loadTransactions: function() {
			self.isLoading = true;
			if (self.accountParam !== 0 && self.accountParam !== "") {
				
				if (self.transactionList.length > 0) {
					self.transactionList.length = 0;
				}
				
				var promise = $http({
					method: "GET",
					url: self.baseUrl+"/all/"+self.accountParam
				})
				.then(function(result) {
					var items = angular.fromJson(result.data);
					angular.forEach(items, function(item) {
						self.transactionList.push(item)
					});
					self.isLoading = false;
				}, function(err) {
					console.error(err);
				});
			} else {
				console.error("No account parameter specified.");
			}
		},
		addTransaction: function(params) {
			var promise = $http({
				method: "POST",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				url: self.baseUrl,
				data: $.param(params)
			});
			return promise;
		},
		setAccountParam: function(param) {
			self.accountParam = param;
		}
	};
	
	return self;
});